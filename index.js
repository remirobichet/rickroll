import TypeIt from "typeit";

var el = document.getElementById('feel-it-comming');

new TypeIt("#feel-it-comming", {
    speed: 75,
    strings: ["Bienvenue sur mon blog voyage ! 😃", "Non jdéconne, j'allais pas partir sans une dernière blague 🙃"],
    afterComplete: (step, instance) => {
        setTimeout(() => {
            el.classList.add('hide');
            var iframe = document.createElement('iframe');
            iframe.src = "https://www.youtube.com/embed/dQw4w9WgXcQ?controls=0&autoplay=true";
            iframe.frameborder = 0;
            iframe.allow = "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture";
            document.body.appendChild(iframe);
        }, 1500)
    }
}).go();
